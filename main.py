#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Spyder Editor
Created on Mon Mar 13 00:27:37 2019
@author: eespejel

20190313
DATA LIT COURSE
6.7 MIDTERM ASSIGNMENT: LINEAR REGRESSION BASED PRICE PREDICTION
BY: EDUARDO ESPEJEL

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
import sys

### Scikit-Learn
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import TheilSenRegressor
from sklearn.linear_model import LassoLars

### AlphaVantage
'''
NOTE:
    The AlphaVantageKeys file contains my API access key.
    Please request a new access key from the Alpha Vantage site.
    5 minute process...    https://www.alphavantage.co/
'''
from lib import alphaVantageKeys as avk
from alpha_vantage.timeseries import TimeSeries

### Command line options handling
from lib.cmdline_options import Options

if __name__ == '__main__':
        
    ### FIRST OF ALL, DEAL WITH THE COMMAND LINE
    ### OBTAIN: LINEAR REGRESSION METHOD (DEFAULT: THE THREE OF THEM), LIST OF STOCK NAMES
    options = Options()
    options.parse( sys.argv[1:] )
    method = options.known.method
    stocksList = options.known.StockName
    
    ### PREPARE ALPHA API ACCESS
    ts = TimeSeries( key = avk.LLAVE_ALPHA_VANTAGE, output_format="pandas")

    '''
    For each stock passsed in the command line:
        1. Obtain Alpha Vantage historical prices
        2. Filter in the daily close prices
        3. Run the selected Regression methods
        4. Lists score values
        5. Display corresponding chart
    '''
    for stockName in stocksList:
        try:
            ### OBTAIN HISTORICAL TIME SERIES FROM ALPHA VANTAGE SERVICE
            data, _ = ts.get_daily( symbol = stockName, outputsize='compact')
        except:
            print("Error: In Alpha Vantage Access. Does the SotckName exist? Aborting...")
            #sys.exit(0)
            raise SystemExit
        originalDates = data.index.tolist()
        data = data.reset_index()
        prices = data['4. close'].tolist()
        dates = data.index.tolist()
    
        #Convert Date & Price to 1-D Vectors
        dates = np.reshape(dates, (len(dates), 1))
        prices = np.reshape(prices, (len(prices), 1))
        prices = np.ravel(prices)
    
        ### Define Initial Scatter plot for Historical Price Display
        style.use('ggplot')
        plt.scatter( dates, prices, color='yellow', label= 'Actual Price')
        
        '''
        For each of the selected Regression methods:
            1. Run the Regression Method
            2. Predict Prices
            3. Show Achieved score value
            3. Send predicted prices to the chart, for visual comparisson sake
        '''
        print("\nAchieved Score(s):")
        if method in ("Simple",    "All"):
            regressor = LinearRegression()
            regressor.fit(dates, prices)
            print("Score {}: {:.4f}".format( "   Simple", regressor.score(dates, prices)))
            plt.plot(dates, regressor.predict(dates), color='red', linewidth=3, label = 'Simple')
        if method in ("TheilSen",  "All"):
            regressor = TheilSenRegressor()
            regressor.fit(dates, prices)
            print("Score {}: {:.4f}".format( " TheilSen", regressor.score(dates, prices)))
            plt.plot(dates, regressor.predict(dates), color='green', linewidth=3, label = 'TheilSen')
        if method in ("LassoLars", "All"):
            regressor = LassoLars(alpha=0.1, 
                                  copy_X=True,
                                  fit_path=True, 
                                  max_iter=500, 
                                  normalize=True, 
                                  positive=False, 
                                  precompute='auto', 
                                  verbose=False)
            regressor.fit(dates, prices)
            print("Score {}: {:.4f}".format( "LassoLars", regressor.score(dates, prices)))
            plt.plot(dates, regressor.predict(dates), color='blue', linewidth=3, label = 'LassoLars')
        
        ### Prepare and display chart
        plt.title(stockName.upper() + ' <--> Linear Regression | Time vs. Price')
        plt.legend()
        plt.xlabel('Date (Integer)')
        plt.rcParams["figure.figsize"] = [9,4.5]
        plt.show()
