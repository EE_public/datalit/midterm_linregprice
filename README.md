# Midterm_LinRegPrice

## DATA LIT COURSE
## Midterm Assignment 6.7: PRICE PREDICTION BASED ON LINEAR REGRESSION
#  By: Eduardo Espejel

# NOTES:

## Usage:
### PROVIDES LINEAR REGRESSION ESTIMATE FOR A GIVEN STOCK, BASED ON THE ASSET'S HISTORICAL CLOSE PRICES

### positional arguments:
    StockName             List of Stock names which prices are to be predicted, e.g. MSFT GOOG FB

### optional arguments:
    -h, --help            show this help message and exit
    -m {Simple,TheilSen,LassoLars,All}, --method {Simple,TheilSen,LassoLars,All}
        Linear regression method to use

## Command Line Examples:

    python APPNAME -m All GE MSFT TSLA
>        Use the three available methods for price-predicting the three indicated assets

    python APPNAME -m LassoLars FB
>        Use LassoLars regressor for price-predict Facebook prices

    python APPNAME GOOG
>        Use the three available methods for price-predicting Google prices

## Dependencies:
1. Alpha Vantage service. Free of Charge basic key can be requested. Go to: [Their web site](https://www.alphavantage.co/)
2. I used [Romel Torres' Alpha Vantage API implementation](https://github.com/RomelTorres/alpha_vantage)
3. He also kindly provided an API usage exmple [here](https://github.com/RomelTorres/av_example)

## Additional Comments:
1. Please notice that a separate file with your Alpha Vantage API keys is required.
2. A pdf sample file is included as an output sample. Enjoy,

