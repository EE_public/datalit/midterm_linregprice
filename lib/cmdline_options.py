#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 14 11:22:52 2019

@author: eespejel
"""

from argparse import ArgumentParser

class Options:

    def __init__(self):
        self._init_parser()

    def _init_parser(self):
        usage="\n\tPROVIDES LINEAR REGRESSION ESTIMATE FOR A GIVEN STOCK,\n\tBASED ON THE ASSET'S HISTORICAL CLOSE PRICES"
        self.parser = ArgumentParser(usage=usage)
        # Parameters description
        self.parser.add_argument( "-m", "--method", 
                                 choices=["Simple", "TheilSen", "LassoLars", "All"], 
                                 default="All", 
                                 help="Linear regression method to use")
        self.parser.add_argument( "StockName",
                                 nargs="+",
                                 help="List of Stock names which prices are to be predicted, e.g. MSFT GOOG FB")
        #self.parser.add_argument( "PredictionDate", 
        #                         help="Date which price is to be predicted, format YYYY-MM-DD")

    def parse(self, args=None):
        # parse known args, returns a Namespace object
        # unknown args are ignored
        # Parse known args returns (Namespace_of_known, list_of_unknown)
        self.parser.print_help()
        self.known, self.unknown = self.parser.parse_known_args(args)[:]
        print("-"*70)
        print("-"*70)
        print("STOCK PRICE PREDICTION BASED ON LINEAR REGRESSION ESTIMATION")
        print("            (USING THE ALPHA VANTAGE SERVICE)")
        print("\tParameters:")
        print("\tLinear Regressor: {}".format(self.known.method))
        print("\tStock to predict:  {}".format(self.known.StockName))
        #print("\tDate to predict:   {}".format(self.known.PredictionDate))
        if len(self.unknown) != 0:
            print('*WARN* Unknown args received (will de ignored): '+repr(self.unknown))
        print("-"*70)
        print("-"*70)
